// Arreglar el formato de una oracion
// 		Una oracion con un formato correcto empieza
// 		con letra inicial mayuscula y lo demas en minuscula.
// Faltan cosas por considerar pero se hara de manera simple.

// Ejemplo:
// El perro es grande.
// el peRRo es grandE.

Algoritmo FormatoOracion
	Definir oracion, primerLetra, resto, oracionCorrecta Como Caracter
	
	Escribir "Escribe una oracion"
	Leer oracion
	
	// Obteniendo la primera letra de un texto
	primerLetra <- SubCadena(oracion, 1, 1)
	resto <- SubCadena(oracion, 2, Longitud(oracion))
	
	oracionCorrecta <- Concatenar(Mayusculas(primerLetra), Minusculas(resto))
	Escribir oracionCorrecta
FinAlgoritmo
