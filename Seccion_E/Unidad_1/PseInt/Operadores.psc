Algoritmo CalculadoraBasica
	Definir A Como Entero
	Definir B Como Entero
	Definir Operador Como Caracter
	Definir Resultado Como Real
	Definir Error Como Logico
	
	Error = Falso
	
	Imprimir 'Ingrese el numero A'
	Leer A
	
	Imprimir 'Ingrese el numero B'
	Leer B
	
	Imprimir 'Ingrese el operador'
	Leer Operador
	
	Si Operador == "+"
		// Operacion de suma
		Resultado = A+B
	SiNo
		Si Operador == "-"
			// Operacion de resta
			Resultado = A-B
		SiNo
			Si Operador == "*"
				// Operacion de multiplicacion
				Resultado = A*B
			SiNo
				Si Operador == "/" Y B <> 0
					// Operacion de division
					Resultado = A/B
				SiNo
					// Error
					Imprimir 'Error'
					Error = Verdadero
				FinSi
			FinSi
		FinSi
		
	FinSi
	
	Si Error == Falso
		Imprimir 'El resultado de la operacion es: ', Resultado
	FinSi
FinAlgoritmo
